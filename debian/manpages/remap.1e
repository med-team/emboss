'\" t
.\"     Title: REMAP
.\"    Author: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
.\" Generator: DocBook XSL Stylesheets v1.76.1 <http://docbook.sf.net/>
.\"      Date: 05/11/2012
.\"    Manual: EMBOSS Manual for Debian
.\"    Source: EMBOSS 6.4.0
.\"  Language: English
.\"
.TH "REMAP" "1e" "05/11/2012" "EMBOSS 6.4.0" "EMBOSS Manual for Debian"
.\" -----------------------------------------------------------------
.\" * Define some portability stuff
.\" -----------------------------------------------------------------
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.\" http://bugs.debian.org/507673
.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.ie \n(.g .ds Aq \(aq
.el       .ds Aq '
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
remap \- Display restriction enzyme binding sites in a nucleotide sequence
.SH "SYNOPSIS"
.HP \w'\fBremap\fR\ 'u
\fBremap\fR \fB\-sequence\ \fR\fB\fIseqall\fR\fR \fB\-mfile\ \fR\fB\fIdatafile\fR\fR \fB\-enzymes\ \fR\fB\fIstring\fR\fR \fB\-sitelen\ \fR\fB\fIinteger\fR\fR [\fB\-mincuts\ \fR\fB\fIinteger\fR\fR] [\fB\-maxcuts\ \fR\fB\fIinteger\fR\fR] [\fB\-single\ \fR\fB\fIboolean\fR\fR] [\fB\-blunt\ \fR\fB\fIboolean\fR\fR] [\fB\-sticky\ \fR\fB\fIboolean\fR\fR] [\fB\-ambiguity\ \fR\fB\fIboolean\fR\fR] [\fB\-plasmid\ \fR\fB\fIboolean\fR\fR] [\fB\-methylation\ \fR\fB\fIboolean\fR\fR] [\fB\-commercial\ \fR\fB\fIboolean\fR\fR] [\fB\-table\ \fR\fB\fIlist\fR\fR] [\fB\-frame\ \fR\fB\fIlist\fR\fR] \fB\-outfile\ \fR\fB\fIoutfile\fR\fR [\fB\-cutlist\ \fR\fB\fIboolean\fR\fR] [\fB\-flatreformat\ \fR\fB\fIboolean\fR\fR] [\fB\-limit\ \fR\fB\fIboolean\fR\fR] \fB\-translation\ \fR\fB\fIboolean\fR\fR \fB\-reverse\ \fR\fB\fIboolean\fR\fR \fB\-orfminsize\ \fR\fB\fIinteger\fR\fR \fB\-uppercase\ \fR\fB\fIrange\fR\fR \fB\-highlight\ \fR\fB\fIrange\fR\fR \fB\-threeletter\ \fR\fB\fIboolean\fR\fR \fB\-number\ \fR\fB\fIboolean\fR\fR \fB\-width\ \fR\fB\fIinteger\fR\fR \fB\-length\ \fR\fB\fIinteger\fR\fR \fB\-margin\ \fR\fB\fIinteger\fR\fR \fB\-name\ \fR\fB\fIboolean\fR\fR \fB\-description\ \fR\fB\fIboolean\fR\fR \fB\-offset\ \fR\fB\fIinteger\fR\fR \fB\-html\ \fR\fB\fIboolean\fR\fR
.HP \w'\fBremap\fR\ 'u
\fBremap\fR \fB\-help\fR
.SH "DESCRIPTION"
.PP
\fBremap\fR
is a command line program from EMBOSS (\(lqthe European Molecular Biology Open Software Suite\(rq)\&. It is part of the "Display,Nucleic:Restriction,Nucleic:Translation" command group(s)\&.
.SH "OPTIONS"
.SS "Input section"
.PP
\fB\-sequence\fR \fIseqall\fR
.RS 4
.RE
.PP
\fB\-mfile\fR \fIdatafile\fR
.RS 4
Default value: Emethylsites\&.dat
.RE
.SS "Required section"
.PP
\fB\-enzymes\fR \fIstring\fR
.RS 4
The name \*(Aqall\*(Aq reads in all enzyme names from the REBASE database\&. You can specify enzymes by giving their names with commas between then, such as: \*(AqHincII,hinfI,ppiI,hindiii\*(Aq\&. The case of the names is not important\&. You can specify a file of enzyme names to read in by giving the name of the file holding the enzyme names with a \*(Aq@\*(Aq character in front of it, for example, \*(Aq@enz\&.list\*(Aq\&. Blank lines and lines starting with a hash character or \*(Aq!\*(Aq are ignored and all other lines are concatenated together with a comma character \*(Aq,\*(Aq and then treated as the list of enzymes to search for\&. An example of a file of enzyme names is: ! my enzymes HincII, ppiII ! other enzymes hindiii HinfI PpiI Default value: all
.RE
.PP
\fB\-sitelen\fR \fIinteger\fR
.RS 4
This sets the minimum length of the restriction enzyme recognition site\&. Any enzymes with sites shorter than this will be ignored\&. Default value: 4
.RE
.SS "Additional section"
.PP
\fB\-mincuts\fR \fIinteger\fR
.RS 4
This sets the minimum number of cuts for any restriction enzyme that will be considered\&. Any enzymes that cut fewer times than this will be ignored\&. Default value: 1
.RE
.PP
\fB\-maxcuts\fR \fIinteger\fR
.RS 4
This sets the maximum number of cuts for any restriction enzyme that will be considered\&. Any enzymes that cut more times than this will be ignored\&. Default value: 2000000000
.RE
.PP
\fB\-single\fR \fIboolean\fR
.RS 4
If this is set then this forces the values of the mincuts and maxcuts qualifiers to both be 1\&. Any other value you may have set them to will be ignored\&. Default value: N
.RE
.PP
\fB\-blunt\fR \fIboolean\fR
.RS 4
This allows those enzymes which cut at the same position on the forward and reverse strands to be considered\&. Default value: Y
.RE
.PP
\fB\-sticky\fR \fIboolean\fR
.RS 4
This allows those enzymes which cut at different positions on the forward and reverse strands, leaving an overhang, to be considered\&. Default value: Y
.RE
.PP
\fB\-ambiguity\fR \fIboolean\fR
.RS 4
This allows those enzymes which have one or more \*(AqN\*(Aq ambiguity codes in their pattern to be considered Default value: Y
.RE
.PP
\fB\-plasmid\fR \fIboolean\fR
.RS 4
If this is set then this allows searches for restriction enzyme recognition site and cut positions that span the end of the sequence to be considered\&. Default value: N
.RE
.PP
\fB\-methylation\fR \fIboolean\fR
.RS 4
If this is set then RE recognition sites will not match methylated bases\&. Default value: N
.RE
.PP
\fB\-commercial\fR \fIboolean\fR
.RS 4
If this is set, then only those enzymes with a commercial supplier will be searched for\&. This qualifier is ignored if you have specified an explicit list of enzymes to search for, rather than searching through \*(Aqall\*(Aq the enzymes in the REBASE database\&. It is assumed that, if you are asking for an explicit enzyme, then you probably know where to get it from and so all enzymes names that you have asked to be searched for, and which cut, will be reported whether or not they have a commercial supplier\&. Default value: Y
.RE
.PP
\fB\-table\fR \fIlist\fR
.RS 4
.RE
.PP
\fB\-frame\fR \fIlist\fR
.RS 4
This allows you to specify the frames that are translated\&. If you are not displaying cut sites on the reverse sense, then the reverse sense translations will not be displayed even if you have requested frames 4, 5 or 6\&. By default, all six frames will be displayed\&. Default value: 6
.RE
.SS "Output section"
.PP
\fB\-outfile\fR \fIoutfile\fR
.RS 4
.RE
.PP
\fB\-cutlist\fR \fIboolean\fR
.RS 4
This produces lists in the output of the enzymes that cut, those that cut but are excluded because that cut fewer times than mincut or more times than maxcut and those enzymes that do not cut\&. Default value: Y
.RE
.PP
\fB\-flatreformat\fR \fIboolean\fR
.RS 4
This changes the output format to one where the recognition site is indicated by a row of \*(Aq===\*(Aq characters and the cut site is pointed to by a \*(Aq>\*(Aq character in the forward sense, or a \*(Aq<\*(Aq in the reverse sense strand\&. Default value: N
.RE
.PP
\fB\-limit\fR \fIboolean\fR
.RS 4
This limits the reporting of enzymes to just one enzyme from each group of isoschizomers\&. The enzyme chosen to represent an isoschizomer group is the prototype indicated in the data file \*(Aqembossre\&.equ\*(Aq, which is created by the program \*(Aqrebaseextract\*(Aq\&. If you prefer different prototypes to be used, make a copy of embossre\&.equ in your home directory and edit it\&. If this value is set to be false then all of the input enzymes will be reported\&. You might like to set this to false if you are supplying an explicit set of enzymes rather than searching \*(Aqall\*(Aq of them\&. Default value: Y
.RE
.PP
\fB\-translation\fR \fIboolean\fR
.RS 4
This displays the 6\-frame translations of the sequence in the output\&. Default value: Y
.RE
.PP
\fB\-reverse\fR \fIboolean\fR
.RS 4
This displays the cut sites and translation of the reverse sense\&. Default value: Y
.RE
.PP
\fB\-orfminsize\fR \fIinteger\fR
.RS 4
This sets the minimum size of Open Reading Frames (ORFs) to display in the translations\&. All other translation regions are masked by changing the amino acids to \*(Aq\-\*(Aq characters\&.
.RE
.PP
\fB\-uppercase\fR \fIrange\fR
.RS 4
Regions to put in uppercase\&. If this is left blank, then the sequence case is left alone\&. A set of regions is specified by a set of pairs of positions\&. The positions are integers\&. They are separated by any non\-digit, non\-alpha character\&. Examples of region specifications are: 24\-45, 56\-78 1:45, 67=99;765\&.\&.888 1,5,8,10,23,45,57,99
.RE
.PP
\fB\-highlight\fR \fIrange\fR
.RS 4
Regions to colour if formatting for HTML\&. If this is left blank, then the sequence is left alone\&. A set of regions is specified by a set of pairs of positions\&. The positions are integers\&. They are followed by any valid HTML font colour\&. Examples of region specifications are: 24\-45 blue 56\-78 orange 1\-100 green 120\-156 red A file of ranges to colour (one range per line) can be specified as \*(Aq@filename\*(Aq\&.
.RE
.PP
\fB\-threeletter\fR \fIboolean\fR
.RS 4
Default value: N
.RE
.PP
\fB\-number\fR \fIboolean\fR
.RS 4
Default value: N
.RE
.PP
\fB\-width\fR \fIinteger\fR
.RS 4
Default value: 60
.RE
.PP
\fB\-length\fR \fIinteger\fR
.RS 4
.RE
.PP
\fB\-margin\fR \fIinteger\fR
.RS 4
Default value: 10
.RE
.PP
\fB\-name\fR \fIboolean\fR
.RS 4
Set this to be false if you do not wish to display the ID name of the sequence Default value: Y
.RE
.PP
\fB\-description\fR \fIboolean\fR
.RS 4
Set this to be false if you do not wish to display the description of the sequence Default value: Y
.RE
.PP
\fB\-offset\fR \fIinteger\fR
.RS 4
Default value: 1
.RE
.PP
\fB\-html\fR \fIboolean\fR
.RS 4
Default value: N
.RE
.SH "BUGS"
.PP
Bugs can be reported to the Debian Bug Tracking system (http://bugs\&.debian\&.org/emboss), or directly to the EMBOSS developers (http://sourceforge\&.net/tracker/?group_id=93650&atid=605031)\&.
.SH "SEE ALSO"
.PP
remap is fully documented via the
\fBtfm\fR(1)
system\&.
.SH "AUTHOR"
.PP
\fBDebian Med Packaging Team\fR <\&debian\-med\-packaging@lists\&.alioth\&.debian\&.org\&>
.RS 4
Wrote the script used to autogenerate this manual page\&.
.RE
.SH "COPYRIGHT"
.br
.PP
This manual page was autogenerated from an Ajax Control Definition of the EMBOSS package\&. It can be redistributed under the same terms as EMBOSS itself\&.
.sp

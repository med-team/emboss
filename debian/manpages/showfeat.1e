'\" t
.\"     Title: SHOWFEAT
.\"    Author: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
.\" Generator: DocBook XSL Stylesheets v1.76.1 <http://docbook.sf.net/>
.\"      Date: 05/11/2012
.\"    Manual: EMBOSS Manual for Debian
.\"    Source: EMBOSS 6.4.0
.\"  Language: English
.\"
.TH "SHOWFEAT" "1e" "05/11/2012" "EMBOSS 6.4.0" "EMBOSS Manual for Debian"
.\" -----------------------------------------------------------------
.\" * Define some portability stuff
.\" -----------------------------------------------------------------
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.\" http://bugs.debian.org/507673
.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.ie \n(.g .ds Aq \(aq
.el       .ds Aq '
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
showfeat \- Display features of a sequence in pretty format
.SH "SYNOPSIS"
.HP \w'\fBshowfeat\fR\ 'u
\fBshowfeat\fR \fB\-sequence\ \fR\fB\fIseqall\fR\fR [\fB\-sourcematch\ \fR\fB\fIstring\fR\fR] [\fB\-typematch\ \fR\fB\fIstring\fR\fR] [\fB\-tagmatch\ \fR\fB\fIstring\fR\fR] [\fB\-valuematch\ \fR\fB\fIstring\fR\fR] [\fB\-sort\ \fR\fB\fIlist\fR\fR] [\fB\-joinfeatures\ \fR\fB\fIboolean\fR\fR] [\fB\-annotation\ \fR\fB\fIrange\fR\fR] \fB\-html\ \fR\fB\fIboolean\fR\fR \fB\-id\ \fR\fB\fIboolean\fR\fR \fB\-description\ \fR\fB\fIboolean\fR\fR \fB\-scale\ \fR\fB\fIboolean\fR\fR \fB\-width\ \fR\fB\fIinteger\fR\fR \fB\-collapse\ \fR\fB\fIboolean\fR\fR \fB\-forward\ \fR\fB\fIboolean\fR\fR \fB\-reverse\ \fR\fB\fIboolean\fR\fR \fB\-unknown\ \fR\fB\fIboolean\fR\fR \fB\-strand\ \fR\fB\fIboolean\fR\fR \fB\-origin\ \fR\fB\fIboolean\fR\fR \fB\-position\ \fR\fB\fIboolean\fR\fR \fB\-type\ \fR\fB\fIboolean\fR\fR \fB\-tags\ \fR\fB\fIboolean\fR\fR \fB\-values\ \fR\fB\fIboolean\fR\fR \fB\-stricttags\ \fR\fB\fIboolean\fR\fR \fB\-outfile\ \fR\fB\fIoutfile\fR\fR
.HP \w'\fBshowfeat\fR\ 'u
\fBshowfeat\fR \fB\-help\fR
.SH "DESCRIPTION"
.PP
\fBshowfeat\fR
is a command line program from EMBOSS (\(lqthe European Molecular Biology Open Software Suite\(rq)\&. It is part of the "Display,Feature tables" command group(s)\&.
.SH "OPTIONS"
.SS "Input section"
.PP
\fB\-sequence\fR \fIseqall\fR
.RS 4
.RE
.SS "Additional section"
.PP
\fB\-sourcematch\fR \fIstring\fR
.RS 4
By default any feature source in the feature table is shown\&. You can set this to match any feature source you wish to show\&. The source name is usually either the name of the program that detected the feature or it is the feature table (eg: EMBL) that the feature came from\&. The source may be wildcarded by using \*(Aq*\*(Aq\&. If you wish to show more than one source, separate their names with the character \*(Aq|\*(Aq, eg: gene* | embl Default value: *
.RE
.PP
\fB\-typematch\fR \fIstring\fR
.RS 4
By default any feature type in the feature table is shown\&. You can set this to match any feature type you wish to show\&. See http://www\&.ebi\&.ac\&.uk/embl/WebFeat/ for a list of the EMBL feature types and see Appendix A of the Swissprot user manual in http://www\&.expasy\&.org/sprot/userman\&.html for a list of the Swissprot feature types\&. The type may be wildcarded by using \*(Aq*\*(Aq\&. If you wish to show more than one type, separate their names with the character \*(Aq|\*(Aq, eg: *UTR | intron Default value: *
.RE
.PP
\fB\-tagmatch\fR \fIstring\fR
.RS 4
Tags are the types of extra values that a feature may have\&. For example in the EMBL feature table, a \*(AqCDS\*(Aq type of feature may have the tags \*(Aq/codon\*(Aq, \*(Aq/codon_start\*(Aq, \*(Aq/db_xref\*(Aq, \*(Aq/EC_number\*(Aq, \*(Aq/evidence\*(Aq, \*(Aq/exception\*(Aq, \*(Aq/function\*(Aq, \*(Aq/gene\*(Aq, \*(Aq/label\*(Aq, \*(Aq/map\*(Aq, \*(Aq/note\*(Aq, \*(Aq/number\*(Aq, \*(Aq/partial\*(Aq, \*(Aq/product\*(Aq, \*(Aq/protein_id\*(Aq, \*(Aq/pseudo\*(Aq, \*(Aq/standard_name\*(Aq, \*(Aq/translation\*(Aq, \*(Aq/transl_except\*(Aq, \*(Aq/transl_table\*(Aq, or \*(Aq/usedin\*(Aq\&. Some of these tags also have values, for example \*(Aq/gene\*(Aq can have the value of the gene name\&. By default any feature tag in the feature table is shown\&. You can set this to match any feature tag you wish to show\&. The tag may be wildcarded by using \*(Aq*\*(Aq\&. If you wish to show more than one tag, separate their names with the character \*(Aq|\*(Aq, eg: gene | label Default value: *
.RE
.PP
\fB\-valuematch\fR \fIstring\fR
.RS 4
Tag values are the values associated with a feature tag\&. Tags are the types of extra values that a feature may have\&. For example in the EMBL feature table, a \*(AqCDS\*(Aq type of feature may have the tags \*(Aq/codon\*(Aq, \*(Aq/codon_start\*(Aq, \*(Aq/db_xref\*(Aq, \*(Aq/EC_number\*(Aq, \*(Aq/evidence\*(Aq, \*(Aq/exception\*(Aq, \*(Aq/function\*(Aq, \*(Aq/gene\*(Aq, \*(Aq/label\*(Aq, \*(Aq/map\*(Aq, \*(Aq/note\*(Aq, \*(Aq/number\*(Aq, \*(Aq/partial\*(Aq, \*(Aq/product\*(Aq, \*(Aq/protein_id\*(Aq, \*(Aq/pseudo\*(Aq, \*(Aq/standard_name\*(Aq, \*(Aq/translation\*(Aq, \*(Aq/transl_except\*(Aq, \*(Aq/transl_table\*(Aq, or \*(Aq/usedin\*(Aq\&. Only some of these tags can have values, for example \*(Aq/gene\*(Aq can have the value of the gene name\&. By default any feature tag value in the feature table is shown\&. You can set this to match any feature tag value you wish to show\&. The tag value may be wildcarded by using \*(Aq*\*(Aq\&. If you wish to show more than one tag value, separate their names with the character \*(Aq|\*(Aq, eg: pax* | 10 Default value: *
.RE
.PP
\fB\-sort\fR \fIlist\fR
.RS 4
Default value: start
.RE
.PP
\fB\-joinfeatures\fR \fIboolean\fR
.RS 4
Default value: N
.RE
.PP
\fB\-annotation\fR \fIrange\fR
.RS 4
Regions to annotate by marking\&. If this is left blank, then no annotation is added\&. A set of regions is specified by a set of pairs of positions followed by optional text\&. The positions are integers\&. They are followed by any text (but not digits when on the command\-line)\&. Examples of region specifications are: 24\-45 new domain 56\-78 match to Mouse 1\-100 First part 120\-156 oligo A file of ranges to annotate (one range per line) can be specified as \*(Aq@filename\*(Aq\&.
.RE
.SS "Advanced section"
.PP
\fB\-html\fR \fIboolean\fR
.RS 4
Default value: N
.RE
.PP
\fB\-id\fR \fIboolean\fR
.RS 4
Set this to be false if you do not wish to display the ID name of the sequence\&. Default value: Y
.RE
.PP
\fB\-description\fR \fIboolean\fR
.RS 4
Set this to be false if you do not wish to display the description of the sequence\&. Default value: Y
.RE
.PP
\fB\-scale\fR \fIboolean\fR
.RS 4
Set this to be false if you do not wish to display the scale line\&. Default value: Y
.RE
.PP
\fB\-width\fR \fIinteger\fR
.RS 4
You can expand (or contract) the width of the ASCII\-character graphics display of the positions of the features using this value\&. For example, a width of 80 characters would cover a standard page width and a width a 10 characters would be nearly unreadable\&. If the width is set to less than 4, the graphics lines and the scale line will not be displayed\&. Default value: 60
.RE
.PP
\fB\-collapse\fR \fIboolean\fR
.RS 4
If this is set, then features from the same source and of the same type and sense are all printed on the same line\&. For instance if there are several features from the EMBL feature table (ie\&. the same source) which are all of type \*(Aqexon\*(Aq in the same sense, then they will all be displayed on the same line\&. This makes it hard to distinguish overlapping features\&. If this is set to false then each feature is displayed on a separate line making it easier to distinguish where features start and end\&. Default value: N
.RE
.PP
\fB\-forward\fR \fIboolean\fR
.RS 4
Set this to be false if you do not wish to display forward sense features\&. Default value: Y
.RE
.PP
\fB\-reverse\fR \fIboolean\fR
.RS 4
Set this to be false if you do not wish to display reverse sense features\&. Default value: Y
.RE
.PP
\fB\-unknown\fR \fIboolean\fR
.RS 4
Set this to be false if you do not wish to display unknown sense features\&. (ie\&. features with no directionality \- all protein features are of this type and some nucleic features (for example, CG\-rich regions))\&. Default value: Y
.RE
.PP
\fB\-strand\fR \fIboolean\fR
.RS 4
Set this if you wish to display the strand of the features\&. Protein features are always directionless (indicated by \*(Aq0\*(Aq), forward is indicated by \*(Aq+\*(Aq and reverse is \*(Aq\-\*(Aq\&. Default value: N
.RE
.PP
\fB\-origin\fR \fIboolean\fR
.RS 4
Set this if you wish to display the origin of the features\&. The source name is usually either the name of the program that detected the feature or it is the name of the feature table (eg: EMBL) that the feature came from\&. Default value: N
.RE
.PP
\fB\-position\fR \fIboolean\fR
.RS 4
Set this if you wish to display the start and end position of the features\&. If several features are being displayed on the same line, then the start and end positions will be joined by a comma, for example: \*(Aq189\-189,225\-225\*(Aq\&. Default value: N
.RE
.PP
\fB\-type\fR \fIboolean\fR
.RS 4
Set this to be false if you do not wish to display the type of the features\&. Default value: Y
.RE
.PP
\fB\-tags\fR \fIboolean\fR
.RS 4
Set this to be false if you do not wish to display the tags and values of the features\&. Default value: N
.RE
.PP
\fB\-values\fR \fIboolean\fR
.RS 4
Set this to be false if you do not wish to display the tag values of the features\&. If this is set to be false, only the tag names will be displayed\&. If the tags are not displayed, then the values will not be displayed\&. The value of the \*(Aqtranslation\*(Aq tag is never displayed as it is often extremely long\&. Default value: Y
.RE
.PP
\fB\-stricttags\fR \fIboolean\fR
.RS 4
By default if any tag/value pair in a feature matches the specified tag and value, then all the tags/value pairs of that feature will be displayed\&. If this is set to be true, then only those tag/value pairs in a feature that match the specified tag and value will be displayed\&. Default value: N
.RE
.SS "Output section"
.PP
\fB\-outfile\fR \fIoutfile\fR
.RS 4
.RE
.SH "BUGS"
.PP
Bugs can be reported to the Debian Bug Tracking system (http://bugs\&.debian\&.org/emboss), or directly to the EMBOSS developers (http://sourceforge\&.net/tracker/?group_id=93650&atid=605031)\&.
.SH "SEE ALSO"
.PP
showfeat is fully documented via the
\fBtfm\fR(1)
system\&.
.SH "AUTHOR"
.PP
\fBDebian Med Packaging Team\fR <\&debian\-med\-packaging@lists\&.alioth\&.debian\&.org\&>
.RS 4
Wrote the script used to autogenerate this manual page\&.
.RE
.SH "COPYRIGHT"
.br
.PP
This manual page was autogenerated from an Ajax Control Definition of the EMBOSS package\&. It can be redistributed under the same terms as EMBOSS itself\&.
.sp

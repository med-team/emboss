'\" t
.\"     Title: PRETTYPLOT
.\"    Author: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
.\" Generator: DocBook XSL Stylesheets v1.76.1 <http://docbook.sf.net/>
.\"      Date: 05/11/2012
.\"    Manual: EMBOSS Manual for Debian
.\"    Source: EMBOSS 6.4.0
.\"  Language: English
.\"
.TH "PRETTYPLOT" "1e" "05/11/2012" "EMBOSS 6.4.0" "EMBOSS Manual for Debian"
.\" -----------------------------------------------------------------
.\" * Define some portability stuff
.\" -----------------------------------------------------------------
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.\" http://bugs.debian.org/507673
.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.ie \n(.g .ds Aq \(aq
.el       .ds Aq '
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
prettyplot \- Draw a sequence alignment with pretty formatting
.SH "SYNOPSIS"
.HP \w'\fBprettyplot\fR\ 'u
\fBprettyplot\fR \fB\-sequences\ \fR\fB\fIseqset\fR\fR [\fB\-matrixfile\ \fR\fB\fImatrix\fR\fR] [\fB\-residuesperline\ \fR\fB\fIinteger\fR\fR] [\fB\-resbreak\ \fR\fB\fIinteger\fR\fR] [\fB\-ccolours\ \fR\fB\fIboolean\fR\fR] [\fB\-cidentity\ \fR\fB\fIstring\fR\fR] [\fB\-csimilarity\ \fR\fB\fIstring\fR\fR] [\fB\-cother\ \fR\fB\fIstring\fR\fR] [\fB\-docolour\ \fR\fB\fIboolean\fR\fR] [\fB\-shade\ \fR\fB\fIstring\fR\fR] [\fB\-pair\ \fR\fB\fIarray\fR\fR] [\fB\-identity\ \fR\fB\fIinteger\fR\fR] [\fB\-box\ \fR\fB\fIboolean\fR\fR] [\fB\-boxcol\ \fR\fB\fIboolean\fR\fR] [\fB\-boxuse\ \fR\fB\fIstring\fR\fR] [\fB\-name\ \fR\fB\fIboolean\fR\fR] [\fB\-maxnamelen\ \fR\fB\fIinteger\fR\fR] [\fB\-number\ \fR\fB\fIboolean\fR\fR] [\fB\-listoptions\ \fR\fB\fIboolean\fR\fR] [\fB\-plurality\ \fR\fB\fIfloat\fR\fR] [\fB\-consensus\ \fR\fB\fIboolean\fR\fR] [\fB\-collision\ \fR\fB\fIboolean\fR\fR] [\fB\-alternative\ \fR\fB\fIlist\fR\fR] [\fB\-showscore\ \fR\fB\fIinteger\fR\fR] [\fB\-portrait\ \fR\fB\fIboolean\fR\fR] \fB\-graph\ \fR\fB\fIgraph\fR\fR
.HP \w'\fBprettyplot\fR\ 'u
\fBprettyplot\fR \fB\-help\fR
.SH "DESCRIPTION"
.PP
\fBprettyplot\fR
is a command line program from EMBOSS (\(lqthe European Molecular Biology Open Software Suite\(rq)\&. It is part of the "Alignment:Multiple,Display" command group(s)\&.
.SH "OPTIONS"
.SS "Input section"
.PP
\fB\-sequences\fR \fIseqset\fR
.RS 4
.RE
.PP
\fB\-matrixfile\fR \fImatrix\fR
.RS 4
This is the scoring matrix file used when comparing sequences\&. By default it is the file \*(AqEBLOSUM62\*(Aq (for proteins) or the file \*(AqEDNAFULL\*(Aq (for nucleic sequences)\&. These files are found in the \*(Aqdata\*(Aq directory of the EMBOSS installation\&.
.RE
.SS "Additional section"
.PP
\fB\-residuesperline\fR \fIinteger\fR
.RS 4
The number of residues to be displayed on each line Default value: 50
.RE
.PP
\fB\-resbreak\fR \fIinteger\fR
.RS 4
Default value: $(residuesperline)
.RE
.PP
\fB\-ccolours\fR \fIboolean\fR
.RS 4
Default value: Y
.RE
.PP
\fB\-cidentity\fR \fIstring\fR
.RS 4
Default value: RED
.RE
.PP
\fB\-csimilarity\fR \fIstring\fR
.RS 4
Default value: GREEN
.RE
.PP
\fB\-cother\fR \fIstring\fR
.RS 4
Default value: BLACK
.RE
.PP
\fB\-docolour\fR \fIboolean\fR
.RS 4
Default value: N
.RE
.PP
\fB\-shade\fR \fIstring\fR
.RS 4
Set to BPLW for normal shading (black, pale, light, white) so for pair = 1\&.5,1\&.0,0\&.5 and shade = BPLW Residues score Colour 1\&.5 or over\&.\&.\&. BLACK (B) 1\&.0 to 1\&.5 \&.\&.\&. BROWN (P) 0\&.5 to 1\&.0 \&.\&.\&. WHEAT (L) under 0\&.5 \&.\&.\&.\&. WHITE (W) The only four letters allowed are BPLW, in any order\&.
.RE
.PP
\fB\-pair\fR \fIarray\fR
.RS 4
Default value: 1\&.5,1\&.0,0\&.5
.RE
.PP
\fB\-identity\fR \fIinteger\fR
.RS 4
.RE
.PP
\fB\-box\fR \fIboolean\fR
.RS 4
Default value: Y
.RE
.PP
\fB\-boxcol\fR \fIboolean\fR
.RS 4
Default value: N
.RE
.PP
\fB\-boxuse\fR \fIstring\fR
.RS 4
Default value: GREY
.RE
.PP
\fB\-name\fR \fIboolean\fR
.RS 4
Default value: Y
.RE
.PP
\fB\-maxnamelen\fR \fIinteger\fR
.RS 4
Default value: 10
.RE
.PP
\fB\-number\fR \fIboolean\fR
.RS 4
Default value: Y
.RE
.PP
\fB\-listoptions\fR \fIboolean\fR
.RS 4
Default value: Y
.RE
.PP
\fB\-plurality\fR \fIfloat\fR
.RS 4
Default value: @( $(sequences\&.totweight) / 2)
.RE
.SS "Consensus section"
.PP
\fB\-consensus\fR \fIboolean\fR
.RS 4
Default value: N
.RE
.PP
\fB\-collision\fR \fIboolean\fR
.RS 4
Default value: Y
.RE
.PP
\fB\-alternative\fR \fIlist\fR
.RS 4
Values are 0:Normal collision check\&. (default) 1:Compares identical scores with the max score found\&. So if any other residue matches the identical score then a collision has occurred\&. 2:If another residue has a greater than or equal to matching score and these do not match then a collision has occurred\&. 3:Checks all those not in the current consensus\&.If any of these give a top score for matching or identical scores then a collision has occured\&.
.RE
.PP
\fB\-showscore\fR \fIinteger\fR
.RS 4
Default value: \-1
.RE
.PP
\fB\-portrait\fR \fIboolean\fR
.RS 4
Default value: N
.RE
.SS "Output section"
.PP
\fB\-graph\fR \fIgraph\fR
.RS 4
.RE
.SH "BUGS"
.PP
Bugs can be reported to the Debian Bug Tracking system (http://bugs\&.debian\&.org/emboss), or directly to the EMBOSS developers (http://sourceforge\&.net/tracker/?group_id=93650&atid=605031)\&.
.SH "SEE ALSO"
.PP
prettyplot is fully documented via the
\fBtfm\fR(1)
system\&.
.SH "AUTHOR"
.PP
\fBDebian Med Packaging Team\fR <\&debian\-med\-packaging@lists\&.alioth\&.debian\&.org\&>
.RS 4
Wrote the script used to autogenerate this manual page\&.
.RE
.SH "COPYRIGHT"
.br
.PP
This manual page was autogenerated from an Ajax Control Definition of the EMBOSS package\&. It can be redistributed under the same terms as EMBOSS itself\&.
.sp
